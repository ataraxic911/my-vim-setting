set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'ScrollColors'
Bundle 'EasyGrep'
Bundle 'scrooloose/nerdtree' 
Bundle 'scrooloose/nerdcommenter'
Bundle 'mbbill/undotree'
Bundle 'Tagbar'
Bundle 'vim-scripts/sessionman.vim'
Bundle 'nathanaelkane/vim-indent-guides'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'vim-scripts/bufexplorer.zip'
Bundle 'L9'
Bundle 'FuzzyFinder'
Bundle 'spf13/vim-colors'
Bundle 'tpope/vim-repeat'
Bundle 'bling/vim-airline'
Bundle 'edkolev/tmuxline.vim'
Bundle 'Shougo/neocomplcache.vim'
Bundle 'vim-scripts/cscope.vim'
Bundle 'sotte/presenting.vim'
Bundle 'panozzaj/vim-autocorrect'
Bundle 'vim-airline/vim-airline-themes'
Bundle 'fatih/vim-go'
""Bundle 'vim-syntastic/syntastic'
Bundle 'artur-shaik/vim-javacomplete2'
Bundle 'Chiel92/vim-autoformat'

augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }

autocmd BufNewFile,BufReadPost *.md set filetype=markdown
autocmd filetype text call AutoCorrect()

let mapleader = ','
let maplocalleader = '_'
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_
map <C-L> <C-W>l<C-W>_
map <C-H> <C-W>h<C-W>_

noremap j gj
noremap k gk

nmap <silent> <leader>/ :set invhlsearch<CR>

filetype on
filetype plugin indent on   " Automatically detect file types.
syntax on                   " Syntax highlighting

"set autowrite                       " Automatically write a file when leaving a modified buffer
set shortmess+=filmnrxoOtT          " Abbrev. of messages (avoids 'hit enter')
set virtualedit=onemore             " Allow for cursor beyond last character
set history=1000                    " Store a ton of history (default is 20)
""set spell                           " Spell checking on
set hidden                          " Allow buffer switching without saving
set iskeyword-=.                    " '.' is an end of word designator
set iskeyword-=#                    " '#' is an end of word designator
set iskeyword-=-                    " '-' is an end of word designator
"set mouse=a
set pastetoggle=<F2>
set tabpagemax=15               " Only show 15 tabs
""set showmode                    " Display the current mode
set t_Co=256
set enc=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf8,prc
set cursorline                  " Highlight current line
set number
"set list
set listchars=tab:»\ ,trail:¤,eol:¬


highlight clear SignColumn      " SignColumn should match background
highlight clear LineNr          " Current line number row will have same background color in relative mode

" Find merge conflict markers
map <leader>fc /\v^[<\|=>]{7}( .*\|$)<CR>
noremap <leader>l :set list!<CR>

" Shortcuts
" Change Working Directory to that of the current file
cmap cwd lcd %:p:h
cmap cd. lcd %:p:h

" Visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv

" Allow using the repeat operator with a visual selection (!)
" http://stackoverflow.com/a/8064607/127816
vnoremap . :normal .<CR>

" For when you forget to sudo.. Really Write the file.
cmap w!! w !sudo tee % >/dev/null

" Some helpers to edit mode
" http://vimcasts.org/e/14
cnoremap %% <C-R>=fnameescape(expand('%:h')).'/'<cr>
map <leader>ew :e %%
map <leader>es :sp %%
map <leader>ev :vsp %%
map <leader>et :tabe %%

" Adjust viewports to the same size
map <Leader>= <C-w>=

" Map <Leader>ff to display all lines with keyword under cursor
" and ask which one to jump to
nmap <Leader>ff [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>

" Easier horizontal scrolling
map zl zL
map zh zH

" Easier formatting
nnoremap <silent> <leader>q gwip


set novisualbell " No blinking
set noerrorbells " No noise.
set vb t_vb= " disable any beeps or flashes on error
set laststatus=2 ""
set showcmd " display an incomplete command in statusline
""set statusline=%<%f\ " custom statusline
""set stl+=[%{&ff}] " show fileformat
""set stl+=%y%m%r%=
""set stl+=%-14.(%l,%c%V%)\ %P

set tabstop=4 " tab size eql 2 spaces
set softtabstop=4
set shiftwidth=4 " default shift width for indents
set expandtab " replace tabs with ${tabstop} spaces
set smarttab "

set backspace=indent,eol,start  " Backspace for dummies
set linespace=0                 " No extra spaces between rows
set number                      " Line numbers on
set showmatch                   " Show matching brackets/parenthesis
set incsearch                   " Find as you type search
set hlsearch                    " Highlight search terms
set winminheight=1              " Windows can be 0 line high
set ignorecase                  " Case insensitive search
set smartcase                   " Case sensitive when uc present
set wildmenu                    " Show list instead of just completing
set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.
set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
set scrolljump=5                " Lines to scroll when cursor leaves screen
set scrolloff=3                 " Minimum lines to keep above and below cursor
set foldenable                  " Auto fold code
set background=dark

set splitbelow
set splitright
""set list " display unprintable characters f12 - switches
""map <silent> <F12> :set invlist<CR>

color molokai " Load a colorscheme

" Tabs
nnoremap <Leader>pt :tabprev<CR>
nnoremap <Leader>nt :tabnext<CR>
nnoremap <Leader>nb :bn<CR>
nnoremap <Leader>pb :bp<CR>

map <Leader>fe  :NERDTreeToggle<CR>
map <Leader>ol  :TagbarToggle<CR>

"NERDTree {    
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
   let g:NERDTreeDirArrows=0
"}
  
" FuF customisations "{{{
   let g:fuf_modesDisable = []
   nnoremap <leader>fh :FufHelp<CR>
   nnoremap <leader>1 :FufTagWithCursorWord<CR>
   nnoremap <leader>11 :FufTag<CR>
   nnoremap <leader>2 :FufFileWithCurrentBufferDir<CR>
   nnoremap <leader>22 :FufFile<CR>
   nnoremap <leader>3 :FufBuffer<CR>
   nnoremap <leader>4 :FufDirWithCurrentBufferDir<CR>
   nnoremap <leader>44 :FufDir<CR>
   nnoremap <leader>5 :FufBufferTag<CR>
   nnoremap <leader>55 :FufBufferTagAll<CR>
   nnoremap <leader>6 :FufMruFile<CR>
   nnoremap <leader>7 :FufLine<CR>
   nnoremap <leader>8 :FufChangeList<CR>
   nnoremap <leader>9 :FufTaggedFile<CR>
"}}}


"Tagbar  {
	"let g:tagbar_ctags_bin = '/nobackup/amaniamr/bin/bin/ctags'
"}

"Ctags {
	set tags =/nobackup/amaniamr/tags,./tags;
"}
"
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
      let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline_theme = 'powerlineish'

""
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplcache.
let g:neocomplcache_enable_at_startup = 1
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'

" Enable heavy features.
" Use camel case completion.
"let g:neocomplcache_enable_camel_case_completion = 1
" Use underbar completion.
"let g:neocomplcache_enable_underbar_completion = 1

" Define dictionary.
let g:neocomplcache_dictionary_filetype_lists = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplcache_keyword_patterns')
    let g:neocomplcache_keyword_patterns = {}
endif
let g:neocomplcache_keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplcache#undo_completion()
inoremap <expr><C-l>     neocomplcache#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplcache#smart_close_popup() . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? neocomplcache#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplcache#close_popup()
inoremap <expr><C-e>  neocomplcache#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplcache#close_popup() : "\<Space>"

" For cursor moving in insert mode(Not recommended)
"inoremap <expr><Left>  neocomplcache#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplcache#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplcache#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplcache#close_popup() . "\<Down>"
" Or set this.
"let g:neocomplcache_enable_cursor_hold_i = 1
" Or set this.
"let g:neocomplcache_enable_insert_char_pre = 1

" AutoComplPop like behavior.
"let g:neocomplcache_enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplcache_enable_auto_select = 1
"let g:neocomplcache_disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplcache_force_omni_patterns')
  let g:neocomplcache_force_omni_patterns = {}
endif
let g:neocomplcache_force_omni_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
let g:neocomplcache_force_omni_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplcache_force_omni_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplcache_force_omni_patterns.perl = '\h\w*->\h\w*\|\h\w*::'

nnoremap <leader>fa :call cscope#findInteractive(expand('<cword>'))<CR>
let g:cscope_open_location = 1

"Syntastic {
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_java_checkers = ['checkstyle']

let g:syntastic_java_checkstyle_classpath="/usr/local/algs4/checkstyle-7.4/checkstyle-7.4-all.jar"
let g:syntastic_java_checkstyle_conf_file="/usr/local/algs4/checkstyle-algs4.xml"
let g:syntastic_java_checkstyle_suppress="/usr/local/algs4/checkstyle-suppressions.xml"


"}

"Auto format {
noremap <leader>af :Autoformat<CR>
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0
"}

"Autocomplete java {
let g:JavaComplete_LibsPath="/usr/lib/jvm/java-8-oracle/jre/lib/rt.jar:/usr/local/algs4/algs4.jar"
autocmd FileType java setlocal omnifunc=javacomplete#Complete
"}
