#! /bin/bash
echo "Installing Powerline fonts"

cd ~
git clone https://github.com/powerline/fonts.git
cd fonts
./install.sh
fc-cache -vf ~/.fonts
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf
mv PowerlineSymbols.otf ~/.fonts
mkdir -p ~/.config/fontconfig/conf.d/
mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/

cd -
rm -rf ~/.vim_backup
rm ~/vimrc_backup
echo "Backing up .vim and .vimrc to .vim_backup vimrc_bkup"
cp -r ~/.vim ~/.vim_backup
cp ~/.vimrc ~/vimrc_bkup
cp ~/my-vim-setting/.vimrc ~/.vimrc
mkdir -p ~/.vim/bundle/vundle
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/vundle
sudo apt-get install cscope
sudo apt-get install exuberant-ctags
vim +BundleClean +BundleInstall
